/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*-  */
errordomain OpusDecoder2Error {
	CREATE,
	DECODE
}

class OpusDecoder2 {
	private const uint HZ_PER_KHZ = 1000;
	private const uint MS_PER_SEC = 1000;
	private uint pcm_sampling_frequency_hz; // 1/s == Hz
	private uint pcm_channels; // 1 or 2
	private uint max_pcm_frame_size_per_channel;
	private Opus.Decoder decoder;
	private int16[] pcm_frame_buffer;

	public OpusDecoder2 (uint pcm_sampling_frequency_hz, bool stereo) throws OpusDecoder2Error {
		assert (pcm_sampling_frequency_hz == 48 * HZ_PER_KHZ); // The only sensible frequency
		this.pcm_sampling_frequency_hz = pcm_sampling_frequency_hz;
		this.pcm_channels = stereo ? 2 : 1;

		this.max_pcm_frame_size_per_channel = this.pcm_sampling_frequency_hz / 120 * MS_PER_SEC; // 120 ms is the maximum PCM frame size supported by opus_decode

		Opus.ErrorCode error_code;
		decoder = new Opus.Decoder ((int32) this.pcm_sampling_frequency_hz, (int) this.pcm_channels,
									out error_code);
		if (error_code != Opus.ErrorCode.OK) {
			throw new OpusDecoder2Error.CREATE (@"OPUS decoder initialization error");
		}
		this.pcm_frame_buffer = new int16[this.max_pcm_frame_size_per_channel * this.pcm_channels];
	}
	
	public unowned int16[] decode (uint8[] opus_frame) throws OpusDecoder2Error {
		const int decode_fec = 0;
		var size = decoder.decode (opus_frame, this.pcm_frame_buffer,
								   (int) this.max_pcm_frame_size_per_channel, decode_fec);
		if (size <= 0) {
			// TODO: Use Opus.strerror to get a better error message
			// Opus.ErrorCode error_code = (Opus.ErrorCode) size;
			throw new OpusDecoder2Error.DECODE (@"OPUS decoder returned $(size)");
		}
		return pcm_frame_buffer[0:size];
	}
}

/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*-  */
[CCode (cheader_filename = "opus/opus.h")]
namespace Opus {
/* Copyright (c) 2010-2011 Xiph.Org Foundation, Skype Limited
   Written by Jean-Marc Valin and Koen Vos */
/*
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions
	are met:

	- Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	- Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
	``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
	LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
	A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
	OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
	EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
	PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
	PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

	[CCode (cname="int", cprefix="OPUS_")]
	public enum ErrorCode {
		OK,
		BAD_ARG,
		BUFFER_TOO_SMALL,
		INTERNAL_ERROR,
		INVALID_PACKET,
		UNIMPLEMENTED,
		INVALID_STATE,
		ALLOC_FAIL
	}

	[CCode (cname="int", cprefix="OPUS_APPLICATION_")]
	public enum Application {
		VOIP,
		AUDIO,
		RESTRICTED_LOWDELAY
	}

	[CCode (array_length = false, array_null_terminated = true)]
	unowned uint8[] strerror (ErrorCode error);

	[CCode (array_length = false, array_null_terminated = true)]
	unowned uint8[] opus_get_version_string ();

	[Compact]
	[CCode (free_function="opus_encoder_destroy")]
	public class Encoder {
		public static int get_size (int channels);
		[CCode (cname="opus_encoder_create")]
		public Encoder (int32 fs, int channels, Application application, out ErrorCode error);
		public int init (int32 fs, int channels, Application application);
		[CCode (cname="opus_encode")]
		public int32 encode ([CCode (array_length = false)] int16[] pcm, 
												 int frame_size, uint8[] data);
		[CCode (cname="opus_encode_float")]
		public int32 encode_float ([CCode (array_length = false)] float[] pcm, 
															 int frame_size, uint8[] data);
		public int ctl (int request, ...);
	}
	
	[CCode (free_function="opus_decoder_destroy")]
	[Compact]
	public class Decoder {
		public static int get_size (int channels);
		[CCode (cname="opus_decoder_create")]
		public Decoder (int32 fs, int channels, out ErrorCode error);
		public int init (int32 fs, int channels);
		[CCode (cname = "opus_decode")]
		public int decode (uint8[] data, 
											 [CCode (array_length = false)] int16[] pcm,
											 int frame_size, int decode_fec);
		[CCode (cname="opus_decode_float")]
		public int decode_float (uint8[] data, int32 len, 
														 [CCode (array_length = false)] float[] pcm,
														 int frame_size, int decode_fec);
		public int ctl (int request, ...);
	}
}

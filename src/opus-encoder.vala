/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*-  */
errordomain OpusEncoder2Error {
	CREATE,
	ENCODE
}

class OpusEncoder2 {
	private const uint HZ_PER_KHZ = 1000;
	private const uint MSEC_PER_SEC = 1000;
	private uint pcm_sampling_frequency_hz; // 1/s == Hz
	private uint pcm_channels; // 1 or 2
	private Opus.Application application;
	private uint max_opus_frame_size;
	private Opus.Encoder encoder;
	private uint8[] opus_frame_buffer;
	
	public OpusEncoder2 (uint pcm_sampling_frequency_hz, bool stereo, Opus.Application application,
											 uint max_opus_frame_size) throws OpusEncoder2Error {
		assert (pcm_sampling_frequency_hz == 48 * HZ_PER_KHZ); // The only sensible frequency
		assert (max_opus_frame_size >= 100); // The minimum is a bit arbitrary here
		this.pcm_sampling_frequency_hz = pcm_sampling_frequency_hz;
		this.pcm_channels = stereo ? 2 : 1;
		this.application = application;
		this.max_opus_frame_size = max_opus_frame_size;

		Opus.ErrorCode error_code;
		encoder = new Opus.Encoder ((int32) this.pcm_sampling_frequency_hz,
									(int) this.pcm_channels, this.application, out error_code);
		if (error_code != Opus.ErrorCode.OK) {
			throw new OpusEncoder2Error.CREATE (@"OPUS encoder initialization error");
		}
		this.opus_frame_buffer = new uint8[this.max_opus_frame_size];
	}
	
	public unowned uint8[] encode (int16[] pcm_frame) throws OpusEncoder2Error {
		var pcm_frame_size_per_channel = pcm_frame.length / pcm_channels;
		// TODO: Check for all allowed values
		// assert(pcm_frame_size_per_channel == );
		var size = encoder.encode (pcm_frame,
								   (int) pcm_frame_size_per_channel,
								   opus_frame_buffer);
		if (size <= 0) {
			// TODO: Use Opus.strerror to get a better error message
			// Opus.ErrorCode error_code = (Opus.ErrorCode) size;
			throw new OpusEncoder2Error.ENCODE (@"OPUS encoder returned $(size)");
		}
		return opus_frame_buffer[0:size];
	}
}
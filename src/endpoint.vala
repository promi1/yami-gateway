/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
public enum ConnectionState
{
  DISCONNECTED,
  CONNECTED,
  SYNCED
}
*/

public errordomain EndpointError {
  DECODER,
  ENCODER
}

public class Endpoint
{
  public string hostname;
  public uint16 port;
  public string username;
  public TlsCertificate cert;
  // private IOStream stream;
  // private ConnectionState state;
  private Yami.Client yami_client;
  private SocketConnection socket_connection;
  // private ConnectionState connection_state;
  private InputStream socket_input;
  private OutputStream socket_output;
  private SocketClient socket_client;
  public Gee.Map<uint32, Gee.Queue<OpusFrame>> session_opus_frame_queues;
  private int sequence_number = 0;

  public Endpoint (string id) throws Error {
    session_opus_frame_queues = new Gee.HashMap<uint32, Gee.Queue<OpusFrame>> ();
    socket_client = new SocketClient ();
    socket_client.tls = true;
    socket_client.event.connect (socket_client_event);
  }

  private static void close_stream_noexcept (IOStream stream) {
    try {
      stream.close ();
    }
    catch (Error e) {
      if (!(e is IOError.CLOSED)) {
        stderr.printf (@"error: could not close stream: $(e.message)\n");
      }
    }
  }
    
  public void connect () {
    var network_address = new NetworkAddress (hostname, port);
    // SocketConnection socket_connection;
    try {
      socket_connection = socket_client.connect (network_address);
    }
    catch (Error e) {
      stderr.printf (@"error: could not connect: $(e.message)\n");
      return;
    }
    // connection_state = ConnectionState.CONNECTED;
    socket_output = socket_connection.output_stream;
    socket_input = socket_connection.input_stream;
    yami_client = new Yami.Client ((data) => {
        try {
          size_t bytes_written;
          socket_output.write_all (data, out bytes_written);
          return true;
        }
        catch (IOError e) {
          stderr.printf (@"error: could not write data to socket: $(e.message)\n");
          return false;
        }
      });
    yami_client.send_version ();
    receive.begin ();
    Timeout.add_seconds (15, () => {
        if (yami_client.send_ping ()) {
          return true;
        }
        else {
          stderr.printf (@"error: could not send ping to socket");
          close_stream_noexcept (socket_connection);
          return false;
        }
      });
  }

  private async void receive () {
    while (!socket_connection.is_closed ()) {
      try {
        var size = yami_client.get_next_read_size ();
        ByteArray all_bytes = new ByteArray ();
        while (all_bytes.len < size) {
          var bytes = yield socket_input.read_bytes_async (size - all_bytes.len);
          if (bytes.length == 0) {
            stdout.printf ("the connection was closed by the server\n");
            close_stream_noexcept (socket_connection);
            break;
          }
          all_bytes.append (Bytes.unref_to_data (bytes));
        }
        if (!yami_client.consume_read_data (Bytes.unref_to_data (ByteArray.free_to_bytes (all_bytes)))) {
          stderr.printf ("error: consuming data failed\n");
          close_stream_noexcept (socket_connection);
          break;
        }
        
        if (yami_client.get_message_type () == Yami.MessageType.VERSION)  {
          stdout.printf ("got version message\n");
          yami_client.send_authenticate (username, "", {});
        }
        else if (yami_client.get_message_type () == Yami.MessageType.UDP_TUNNEL) {
          stdout.printf ("got udp tunnel message\n");
          yami_client.recv_udp_tunnel ((p) => {
              if (p.type == Yami.UdpPayloadType.OPUS_AUDIO) {
                Gee.Queue<OpusFrame> queue = null;
                var key = p.source_session_id;
                if (session_opus_frame_queues.has_key (key)) {
                  queue = session_opus_frame_queues.get (key);
                }
                else {
                  queue = new Gee.ArrayQueue<OpusFrame> ();
                  session_opus_frame_queues.set (key, queue);
                }
                queue.offer (new OpusFrame (p.payload, p.terminator));
              }
            });
        }
        else {
          stdout.printf ("got other message\n");
        }
      }
      catch (Error e) {
        if (!(e is IOError.CLOSED)) {
          stderr.printf (@"error: could not read data from socket: $(e.message)\n");
          close_stream_noexcept (socket_connection);
          break;
        }
      }
    }
  }
  
  public void send_opus_frame (uint8[] frame, int _10_ms_count, bool reset) {
    var packet = new Yami.UdpPacket ();
    packet.type = Yami.UdpPayloadType.OPUS_AUDIO;
    packet.target = 0;
    packet.sequence_number = sequence_number;
    packet.payload = frame;
    packet.terminator = reset;
    yami_client.send_udp_tunnel (packet);
    sequence_number += _10_ms_count;
    if (reset) {
      sequence_number = 0;
      
    }
  }
  
  public void socket_client_event (SocketClientEvent event,
                                   SocketConnectable connectable,
                                   IOStream? connection) {
    if (event == SocketClientEvent.TLS_HANDSHAKING) {
		var conn = (TlsClientConnection) connection;
		conn.set_certificate (cert);
		conn.accept_certificate.connect (connection_accept_certificate);
    }
  }

  public bool connection_accept_certificate (TlsCertificate certificate,
                                             TlsCertificateFlags flags) {
    if (flags != 0) {
      // Ignore invalid certificates for now, but at least warn the user
      stdout.printf ("warning: server does not have a strong certificate\n");
    }
    return true;
  }
}

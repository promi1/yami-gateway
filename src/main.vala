/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/*
  Mumble to Mumble gateway
  Copyright (c) 2017 Phobos (promi) <prometheus@unterderbruecke.de>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class Main : Application
{
  private const uint HZ_PER_KHZ = 1000;
  private const uint MSEC_PER_SEC = 1000;
  private const uint pcm_sampling_frequency_hz = 48 * HZ_PER_KHZ;
  private const bool stereo = false;
  private const uint max_opus_frame_size = 7200; // Bytes

  private Gee.List<Endpoint> endpoints;
  private Gee.Map<Gee.Queue, OpusDecoder2> queue_decoders;
  private Gee.Map<Endpoint, OpusEncoder2> endpoint_encoders;
  private string[] args;

  public static int main (string[] args) {
    if (args.length < 11) {
      stderr.printf ("Usage: %s" +
                     " hostname1 port1 username1 cert1.crt key1.key" +
                     " hostname2 port2 username2 cert2.crt cert2.key\n", args[0]);
      return 1;
    }
    var app = new Main (args);
    return app.run ();
  }

  public Main (string[] args) {
    this.args = args;
    
    endpoints = new Gee.ArrayList<Endpoint> ();
    queue_decoders = new Gee.HashMap<Gee.Queue, OpusDecoder2> ();
    endpoint_encoders = new Gee.HashMap<Endpoint, OpusEncoder2> ();
  }

  public override void activate () {
    hold ();
    try {
      create_endpoint (1, "endpoint1");
      create_endpoint (6, "endpoint2");
      create_endpoint_encoders ();
      Timeout.add (60, tunnel);
    }
    catch (Error e) {
      stderr.printf (@"$(e.message)\n");
      release ();
    }
  }

  private void create_endpoint (int args_offset, string id) throws Error {
    var endpoint = new Endpoint (id);
    unowned string[] args_slice = args[args_offset:5];
    endpoint.hostname = args_slice[0];
    endpoint.port = (uint16) int.parse (args_slice[1]);
    endpoint.username = args_slice[2];
    endpoint.cert = new TlsCertificate.from_files (args_slice[3], args_slice[4]);
    endpoint.connect ();
    endpoints.add (endpoint);
  }

  private void create_endpoint_encoders () throws OpusEncoder2Error {
    foreach (var endpoint in endpoints) {
      var encoder = new OpusEncoder2 (pcm_sampling_frequency_hz, stereo,
                                      Opus.Application.VOIP, max_opus_frame_size);
      endpoint_encoders.set (endpoint, encoder);
    }
  }

  private bool tunnel () {
    foreach (var source in endpoints) {
      var mixed_samples =
        new int16[pcm_sampling_frequency_hz * 60 / MSEC_PER_SEC]; // 60 ms pcm frames (mono)
      bool silence = true;
      mix_source (source, ref mixed_samples, ref silence);
      if (silence) {
        continue;
      }
      foreach (var target in endpoints) {
        if (source != target) {
          try {
            var encoded = endpoint_encoders.get (target).encode (mixed_samples);
            target.send_opus_frame (encoded, 6, false);
          }
          catch (Error e) {
            stderr.printf (@"$(e.message)\n");
          }
        }
      }
    }
    return true;
  }

  private void mix_source (Endpoint source, ref int16[] mixed_samples, ref bool silence) {
    unowned int16[] tmp_mixed_samples = mixed_samples;
    unowned bool tmp_silence = silence;
    source.session_opus_frame_queues.foreach ((entry) => {
        var queue = entry.value;
        try {
          var decoder = get_decoder (queue);
          var offset = 0;
          OpusFrame opus_frame;
          while ((opus_frame = queue.poll ()) != null) {
            tmp_silence = false;
            unowned int16[] tmp = null;
            try {
              tmp = decoder.decode (opus_frame.data);
            }
            catch (Error e) {
              stderr.printf (@"Error $(e.message)\n");
              return false;
            }
            for (int i = 0; i < tmp.length; i++) {
              // Ignore overruns
              if (offset + i < tmp_mixed_samples.length) {
                tmp_mixed_samples[offset + i] += tmp[i];
              }
            }
            offset += tmp.length;
          }
        }
        catch (Error e) {
          stderr.printf (@"Error $(e.message)\n");
        }
        return true;
      });
    mixed_samples = tmp_mixed_samples;
    silence = tmp_silence;
  }

  private OpusDecoder2 get_decoder (Gee.Queue<OpusFrame> queue) throws OpusDecoder2Error {
    OpusDecoder2 decoder = null;
    if (queue_decoders.has_key (queue)) {
      decoder = queue_decoders.get (queue);
    }
    else {
      decoder = new OpusDecoder2 (pcm_sampling_frequency_hz, stereo);
      queue_decoders.set (queue, decoder);
    }
    return decoder;
  }
}


/*
  Timeout.add (2000, () => {
  while (true) {
  try {
  var dis1 = Utils.open_read ("test.raw");
  // var dos2 = Utils.open_replace ("test2.raw");

  var encoder = new OpusEncoder2 (pcm_sampling_frequency_hz, stereo, Opus.Application.VOIP,
  pcm_frame_length_ms, max_opus_frame_size);
  // var decoder = new OpusDecoder2 (pcm_sampling_frequency_hz, stereo);
  var max_n = pcm_sampling_frequency_hz / MSEC_PER_SEC *  pcm_frame_length_ms;
  var samples = new int16[max_n];
  while (true) {
  for (int n = 0; n < max_n; n++) {
  samples[n] = dis1.read_int16 ();
  }
  endpoint2.send_opus_frame (encoder.encode (samples));
  Thread.usleep ((pcm_frame_length_ms - 1) * 1000);
  }
  }
  catch (Error e) {
  stdout.printf ("done!\n");
  }
  }
  return false;
  });
*/

/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*-  */
public class PcmFrame {
  public int16[] data;
  public PcmFrame (int16[] data) {
    this.data = data;
  }
}

public class OpusFrame {
  public uint8[] data;
	public bool terminator;
  public OpusFrame (uint8[] data, bool terminator) {
    this.data = data;
		this.terminator = terminator;
  }
}

/*
class Utils {
	public static DataInputStream open_read (string filename) throws Error {
    File file = File.new_for_path (filename);
    var stream = file.read ();
    var dis = new DataInputStream (stream);
    dis.byte_order = DataStreamByteOrder.LITTLE_ENDIAN;
    return dis;
  }

  public static  DataOutputStream open_replace (string filename) throws Error {
    File file = File.new_for_path (filename);
    if (file.query_exists ()) {
      file.delete ();
    }
    var stream = file.replace (null, false, FileCreateFlags.NONE);
    var dos = new DataOutputStream (stream);
    dos.byte_order = DataStreamByteOrder.LITTLE_ENDIAN;
    return dos;
  }
}
*/